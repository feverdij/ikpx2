#!/usr/bin/env python3

import apgmera.lifelib
import sys

if __name__ == "__main__":
    sess = apgmera.lifelib.load_rules("b3s23")
    lt = sess.lifetree()
    pat_col = []
    if len(sys.argv) > 1:
        with open(sys.argv[1],'r') as file_rle:
            record = 0
            rle = ''
            for line_str in file_rle:
                ls = line_str.rstrip('\n')
                if (record == 1):
                    rle += ls
                    if ('!' in ls):
                        record = 0
                        pat = lt.pattern(rle)
                        pat_col.append(pat.apgcode)
                if ('x' in ls):
                    rle = ''
                    record = 1

        unique_pat_col = set(pat_col)
        for apg in unique_pat_col:
            pat = lt.pattern(apg)
            print(apg)
            print(pat.rle_string())  
        print(len(unique_pat_col), ' unique patterns')


#include <set>
#include "ikpxtree.hpp"

void print_help(std::string exe) {
	std::cout << "Usage: " << exe << " inputfile outputfile" << std::endl; 
}

int main (int argc, char* argv[]) {

    std::string exe(argv[0]);

    switch (argc)
    {
        case 1 :
        case 2 :
            print_help(exe);
            return 0;
            break;
        case 3 :
            break;
        default :
            ERREXIT("please specify 2 arguments");
    }   

    std::string inputfile(argv[1]);
    std::string outputfile(argv[2]);

    ikpxtree treegraph(0);

    int i = treegraph.file_format(inputfile);

    if (i == 0) {

        treegraph.read_from_stream(inputfile);
        FILE* fptr = fopen(outputfile.c_str(), "wb");

        if (fptr == NULL) {
            ERREXIT("cannot open backup file " << outputfile << " for writing.");
        }

        treegraph.write_to_file(fptr);
        fclose(fptr);

    } else if (i == 1) {

        FILE* fptr = fopen(inputfile.c_str(), "rb");

        if (fptr == NULL) {
            ERREXIT("cannot open backup file " << inputfile << " for reading.");
        }

        treegraph.read_from_file(fptr);
        fclose(fptr);
	    treegraph.write_to_stream(outputfile);
	
    } else {
    
        ERREXIT("unknown backup file format.");

    }

}
